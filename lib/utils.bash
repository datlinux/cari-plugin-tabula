#!/usr/bin/env bash

set -euo pipefail

GH_REPO="https://github.com/tabulapdf/tabula"
TOOL_NAME="tabula"
TOOL_TEST="tabula"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

curl_opts=(-fSL#)

if [ -n "${GITHUB_API_TOKEN:-}" ]; then
  curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
fi

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | awk '{print $2}'
}

list_github_tags() {
  git ls-remote --tags --refs "$GH_REPO" |
    grep -o 'refs/tags/.*' | cut -d/ -f3- |
    sed 's/^v//'
}

list_all_versions() {  
	list_github_tags
}

download_release() {
  local version filename url
  version="$1"
  filename="$2"
  rm -rf "$filename" &
  url="$GH_REPO/releases/download/v${version}/tabula-jar-${version}.zip"
  echo "* Downloading $TOOL_NAME release $version..."
  curl "${curl_opts[@]}" -o "$filename" -C - "$url" || fail "Could not download $url"
  printf "\\n"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only"
  fi

  (
    mkdir -p "$install_path/bin"
    cp -r "$CARI_DOWNLOAD_PATH"/* "$install_path"
    touch "$install_path/bin/tabula"
    echo "#!/bin/bash" >> "$install_path/bin/tabula"
    echo "if [ -z /usr/lib/jvm/java-11-openjdk-amd64/bin/java ]; then" >> "$install_path/bin/tabula"
    echo "  java -Dfile.encoding=utf-8 -Dwarbler.port=8181 -Dtabula.openBrowser=true -Xms256M -Xmx1024M -jar $install_path/tabula/tabula.jar" >> "$install_path/bin/tabula"
    echo "else " >> "$install_path/bin/tabula"
    echo "  /usr/lib/jvm/java-11-openjdk-amd64/bin/java -Dfile.encoding=utf-8 -Dwarbler.port=8181 -Dtabula.openBrowser=true -Xms256M -Xmx1024M -jar $install_path/tabula/tabula.jar" >> "$install_path/bin/tabula"
    echo "fi" >> "$install_path/bin/tabula"
    chmod a+x "$install_path/bin/tabula"
    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
